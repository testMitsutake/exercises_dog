
class Dog {
	private String Name;
	private int age;
	private String type;


	Dog(String type){
		this.type = type;
	}
	public void SetName(String nm) {
		Name = nm;
	}

	public void SetAge(int age) {
		this.age = age;
	}
	public void ShowProfile() {

		System.out.println("名前は、" + Name + "です。");
		System.out.println("年齢は、" + age + "歳です。");
		System.out.println("犬種は" + type + "です。");
	}

}
