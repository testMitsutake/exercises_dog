
class main {

	public static void main(String[] args) {

		Dog dog1 = new Dog("チワワ");
		Dog dog2 = new Dog("トイプードル");

		dog1.SetName("poti");
		dog2.SetName("tarou");
		dog1.SetAge(3);
		dog2.SetAge(5);
		dog1.ShowProfile();
		dog2.ShowProfile();
	}

}
